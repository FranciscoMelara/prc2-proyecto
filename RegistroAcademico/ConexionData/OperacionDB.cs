﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace ConexionData
{
    public class OperacionDB:Conexion,Operaciones
    {
        private Int32 ejecutarsentencia(String psentencia)
        {
            Int32 filasAfectadas = 0;
            try
            {
                if(base.Conectar())
                {
                    MySqlCommand comando = new MySqlCommand();
                    comando.Connection = base._Conexion;
                    comando.CommandText = psentencia;
                    filasAfectadas = comando.ExecuteNonQuery();
                }
                else
                {
                    filasAfectadas = 0;
                }
            }
            catch
            {
                filasAfectadas = 0;
            }
            return filasAfectadas; 
        }
        private DataTable ejecutarconsulta (String pconsulta)
        {
            DataTable Resultado = new DataTable();
            try
            {
                if(base.Conectar())
                {
                    MySqlCommand comando = new MySqlCommand();
                    MySqlDataAdapter adaptador = new MySqlDataAdapter();
                    comando.Connection = base._Conexion;
                    comando.CommandText = pconsulta;
                    adaptador.SelectCommand = comando;
                    adaptador.Fill(Resultado);
                }
            }
            catch(MySqlException Ex)
            {

            }
            return Resultado;
        }

        public int Insertar(String psentencia)
        {
            return ejecutarsentencia(psentencia);
        }
        public int Modificar(String psentencia)
        {
            return ejecutarsentencia(psentencia);
        }
        public int Eliminar(String psentencia)
        {
            return ejecutarsentencia(psentencia);
        }
        public DataTable Consultar (String pconsulta)
        {
            return ejecutarconsulta(pconsulta);
        }
    }
}
