﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace ConexionData
{
    public class Conexion
    {
       protected MySqlConnection _Conexion = new MySqlConnection();
        
        protected Boolean Conectar()
        {
            Boolean conectado=false;
            String cadenaconexion="Server=localhost;Port=3306;Database=registroacademico;Uid=root;Pwd=root;";
            _Conexion = new MySqlConnection(cadenaconexion);
            try
            {
                _Conexion.Open();
                conectado = true;
            }
            catch
            {
                conectado = false;
            }
            return conectado;
        }
        protected void Desconetar()
        {
            if(_Conexion.State==System.Data.ConnectionState.Open)
            {
                _Conexion.Close();
            }
        }

     }
    
}
