﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegistroAcademico
{
    class AppManager:ApplicationContext
    {
        public AppManager()
        {
            Splash();
            if(login())
            {
                MDIprincipal frm = new MDIprincipal();
                frm.ShowDialog();
            }
        }
        private void Splash()
        {
            frmSplash frm = new frmSplash();
            frm.ShowDialog();
        }
        private Boolean login()
        {
            Boolean validado = false;
            frmLogin frm = new frmLogin();
            frm.ShowDialog();
            validado = frm.Autorizado;
            return validado;
        }
    }
}
