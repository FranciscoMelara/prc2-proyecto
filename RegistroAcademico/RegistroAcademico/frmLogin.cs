﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegistroAcademico
{
    public partial class frmLogin : Form
    {
        Sesion.SesionMa _SESION = Sesion.SesionMa.Instancia;
        
        Boolean _Autorizado = false;

        public Boolean Autorizado
        {
            get { return _Autorizado; }
            //set { _Autorizado = value; }
        }

        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            DataTable resultado = new DataTable();
            Cache.CacheM Consulta = new Cache.CacheM();
            try
            {
                Consulta.Filtro = "Nom_usuario='" + txtUsuario.Text + "' AND Credencial=md5('" + txtCredencial.Text + "')";
                resultado = Consulta.Obtener(Cache.CacheM.Consultas.INICIAR_SESION);
                if(resultado.Rows.Count==1)
                {
                    _Autorizado = true;
                    Close();
                    _SESION.Usuario = resultado.Rows[0]["Nom_usuario"].ToString(); 

                }
                else
                {
                    _Autorizado = false;
                    if(txtUsuario.Text=="" || txtCredencial.Text=="" )
                    {
                        //MessageBox.Show("DATOS EN BLANCO, INTENTE DE NUEVO","AVISO",MessageBoxButtons.OK);
                        lblAstUsu.Visible = true;
                        lblAstCre.Visible = true;
                        lblCamposReq.Visible = true;
                        txtUsuario.Focus();

                    }
                    else
                    {
                        MessageBox.Show("Usuario/Contreseña Erroneos");
                        txtCredencial.Focus();
                    }
                    
                }
            }
            catch
            {

            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.Enter)
            {
                txtCredencial.Focus();
            }
        }

        private void txtCredencial_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCredencial_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.Enter)
            {
                btnAceptar.PerformClick();
            }
        }

    }
}
