﻿namespace RegistroAcademico
{
    partial class MDIprincipal
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MDIprincipal));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.gestionarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aÑOLECTIVOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uSUARIOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gRADOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sECCIONESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aLUMNOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dOCENTESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mATERIASToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pERIDODOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.procesosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iNGRESODENOTASToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportesInformesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuracionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.lblUsuario = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.lblEstacion = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gestionarToolStripMenuItem,
            this.procesosToolStripMenuItem,
            this.reportesInformesToolStripMenuItem,
            this.configuracionToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(787, 25);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // gestionarToolStripMenuItem
            // 
            this.gestionarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aÑOLECTIVOToolStripMenuItem,
            this.uSUARIOSToolStripMenuItem,
            this.gRADOSToolStripMenuItem,
            this.sECCIONESToolStripMenuItem,
            this.aLUMNOSToolStripMenuItem,
            this.dOCENTESToolStripMenuItem,
            this.mATERIASToolStripMenuItem,
            this.pERIDODOSToolStripMenuItem});
            this.gestionarToolStripMenuItem.Font = new System.Drawing.Font("Tw Cen MT", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gestionarToolStripMenuItem.Name = "gestionarToolStripMenuItem";
            this.gestionarToolStripMenuItem.Size = new System.Drawing.Size(80, 21);
            this.gestionarToolStripMenuItem.Text = "Gestionar";
            // 
            // aÑOLECTIVOToolStripMenuItem
            // 
            this.aÑOLECTIVOToolStripMenuItem.Font = new System.Drawing.Font("Tw Cen MT", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aÑOLECTIVOToolStripMenuItem.Name = "aÑOLECTIVOToolStripMenuItem";
            this.aÑOLECTIVOToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.aÑOLECTIVOToolStripMenuItem.Text = "LECTIVO";
            // 
            // uSUARIOSToolStripMenuItem
            // 
            this.uSUARIOSToolStripMenuItem.Font = new System.Drawing.Font("Tw Cen MT", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uSUARIOSToolStripMenuItem.Name = "uSUARIOSToolStripMenuItem";
            this.uSUARIOSToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.uSUARIOSToolStripMenuItem.Text = "USUARIOS";
            this.uSUARIOSToolStripMenuItem.Click += new System.EventHandler(this.uSUARIOSToolStripMenuItem_Click);
            // 
            // gRADOSToolStripMenuItem
            // 
            this.gRADOSToolStripMenuItem.Font = new System.Drawing.Font("Tw Cen MT", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gRADOSToolStripMenuItem.Name = "gRADOSToolStripMenuItem";
            this.gRADOSToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.gRADOSToolStripMenuItem.Text = "GRADOS";
            this.gRADOSToolStripMenuItem.Click += new System.EventHandler(this.gRADOSToolStripMenuItem_Click);
            // 
            // sECCIONESToolStripMenuItem
            // 
            this.sECCIONESToolStripMenuItem.Font = new System.Drawing.Font("Tw Cen MT", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sECCIONESToolStripMenuItem.Name = "sECCIONESToolStripMenuItem";
            this.sECCIONESToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.sECCIONESToolStripMenuItem.Text = "SECCIONES";
            this.sECCIONESToolStripMenuItem.Click += new System.EventHandler(this.sECCIONESToolStripMenuItem_Click);
            // 
            // aLUMNOSToolStripMenuItem
            // 
            this.aLUMNOSToolStripMenuItem.Font = new System.Drawing.Font("Tw Cen MT", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aLUMNOSToolStripMenuItem.Name = "aLUMNOSToolStripMenuItem";
            this.aLUMNOSToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.aLUMNOSToolStripMenuItem.Text = "ALUMNOS";
            this.aLUMNOSToolStripMenuItem.Click += new System.EventHandler(this.aLUMNOSToolStripMenuItem_Click);
            // 
            // dOCENTESToolStripMenuItem
            // 
            this.dOCENTESToolStripMenuItem.Font = new System.Drawing.Font("Tw Cen MT", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dOCENTESToolStripMenuItem.Name = "dOCENTESToolStripMenuItem";
            this.dOCENTESToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.dOCENTESToolStripMenuItem.Text = "MAESTROS";
            this.dOCENTESToolStripMenuItem.Click += new System.EventHandler(this.dOCENTESToolStripMenuItem_Click);
            // 
            // mATERIASToolStripMenuItem
            // 
            this.mATERIASToolStripMenuItem.Font = new System.Drawing.Font("Tw Cen MT", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mATERIASToolStripMenuItem.Name = "mATERIASToolStripMenuItem";
            this.mATERIASToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.mATERIASToolStripMenuItem.Text = "MATERIAS";
            // 
            // pERIDODOSToolStripMenuItem
            // 
            this.pERIDODOSToolStripMenuItem.Font = new System.Drawing.Font("Tw Cen MT", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pERIDODOSToolStripMenuItem.Name = "pERIDODOSToolStripMenuItem";
            this.pERIDODOSToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.pERIDODOSToolStripMenuItem.Text = "PERÍODOS";
            // 
            // procesosToolStripMenuItem
            // 
            this.procesosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iNGRESODENOTASToolStripMenuItem});
            this.procesosToolStripMenuItem.Font = new System.Drawing.Font("Tw Cen MT", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.procesosToolStripMenuItem.Name = "procesosToolStripMenuItem";
            this.procesosToolStripMenuItem.Size = new System.Drawing.Size(73, 21);
            this.procesosToolStripMenuItem.Text = "Procesos";
            // 
            // iNGRESODENOTASToolStripMenuItem
            // 
            this.iNGRESODENOTASToolStripMenuItem.Name = "iNGRESODENOTASToolStripMenuItem";
            this.iNGRESODENOTASToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.iNGRESODENOTASToolStripMenuItem.Text = "INGRESO DE NOTAS";
            // 
            // reportesInformesToolStripMenuItem
            // 
            this.reportesInformesToolStripMenuItem.Font = new System.Drawing.Font("Tw Cen MT", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reportesInformesToolStripMenuItem.Name = "reportesInformesToolStripMenuItem";
            this.reportesInformesToolStripMenuItem.Size = new System.Drawing.Size(133, 21);
            this.reportesInformesToolStripMenuItem.Text = "Reportes/Informes";
            this.reportesInformesToolStripMenuItem.Click += new System.EventHandler(this.reportesInformesToolStripMenuItem_Click);
            // 
            // configuracionToolStripMenuItem
            // 
            this.configuracionToolStripMenuItem.Font = new System.Drawing.Font("Tw Cen MT", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configuracionToolStripMenuItem.Name = "configuracionToolStripMenuItem";
            this.configuracionToolStripMenuItem.Size = new System.Drawing.Size(104, 21);
            this.configuracionToolStripMenuItem.Text = "Configuracion";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Font = new System.Drawing.Font("Tw Cen MT", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(47, 21);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblUsuario,
            this.lblEstacion});
            this.statusStrip.Location = new System.Drawing.Point(0, 423);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(787, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = false;
            this.lblUsuario.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(200, 17);
            this.lblUsuario.Text = "Usuario";
            this.lblUsuario.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblEstacion
            // 
            this.lblEstacion.AutoSize = false;
            this.lblEstacion.ForeColor = System.Drawing.Color.Black;
            this.lblEstacion.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.lblEstacion.Name = "lblEstacion";
            this.lblEstacion.Size = new System.Drawing.Size(200, 17);
            this.lblEstacion.Text = "Estacion";
            // 
            // MDIprincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(787, 445);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MDIprincipal";
            this.Text = "MDIprincipal";
            this.Load += new System.EventHandler(this.MDIprincipal_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel lblUsuario;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem gestionarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem procesosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportesInformesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configuracionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aÑOLECTIVOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gRADOSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sECCIONESToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uSUARIOSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dOCENTESToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mATERIASToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pERIDODOSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iNGRESODENOTASToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aLUMNOSToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel lblEstacion;
    }
}



