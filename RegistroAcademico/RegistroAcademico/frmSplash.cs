﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace RegistroAcademico
{
    public partial class frmSplash : Form
    {
        public frmSplash()
        {
            InitializeComponent();
        }

        private void frmSplash_Load(object sender, EventArgs e)
        {
            tCronometro.Start();
            if(tCronometro.Interval==2000)
            {

            }
        }

        private void tCronometro_Tick(object sender, EventArgs e)
        {
            tCronometro.Stop();
            Close();
        }
    }
}
