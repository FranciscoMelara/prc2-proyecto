﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cache
{
    public class CacheM
    {
        String _Filtro;

        public String Filtro
        {
            get { return _Filtro; }
            set { _Filtro = value; }
        }
        public enum Consultas
        {
            INICIAR_SESION,
        }
        public DataTable Obtener(Consultas pconsulta)
        {
            DataTable Resultado = new DataTable();
            StringBuilder Consulta = new StringBuilder();
            switch(pconsulta)
            {
                case Consultas.INICIAR_SESION:
                    Consulta.Append("SELECT Nom_usuario, Credencial FROM usuarios");
                    Consulta.Append (" where "+_Filtro+";");
                    break;
            }
            try
            {
                ConexionData.OperacionDB operacion = new ConexionData.OperacionDB();
                Resultado = operacion.Consultar(Consulta.ToString());
            }
            catch
            {

            }
            return Resultado;
        }

    }
}
